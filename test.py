import torch
from torch import tensor as t
import numpy as np
import matplotlib.pyplot as plt
import functions_total as f
import model as md

dataname = input('insert the path of the data: ')

rawdata = np.load(dataname)
input_data = t(rawdata['arr_1'],dtype = torch.float)
output_data = t(rawdata['arr_0'],dtype = torch.float)
labelname = input('Insert the path of the labels: ')
labels = t(np.load(labelname)['arr_0'],dtype = torch.float)

input_data, output_data, train_factors, train_factorso = f.normalizeData(input_data,output_data,(0.,1.))

model = md.model
model.load_state_dict(torch.load('model'))
accuracy = 0.
model.eval()
for i in range(len(input_data)) :
  accuracy = accuracy + torch.nn.functional.mse_loss(model(input_data[i]),output_data[i],reduction = 'sum')

print(accuracy/len(input_data))

