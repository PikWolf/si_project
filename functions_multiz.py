import numpy as np 

y,x = np.indices((256,256))
q  = np.sqrt((x-127)**2+(y-127)**2)
q = q.astype(np.int) # ? 
qa = q.ravel()
nq = np.bincount(qa)
q = q.astype(np.float)

def azimuthalAverageFast(immarray) :
 """This function basically do the same as the other azimuthal average,
 but here the r matrix is passed as an argument, so that can be computed 
 only once"""
 tbin = np.bincount(qa,immarray.ravel())
 return tbin/nq

vectAv = np.vectorize(azimuthalAverageFast, signature = '(n,n)->(m)')

def q_filter(qro) : 
  return 1/(1 + (q/qro)**4)

c2,gamma = 0.01,1.5

def tf_filter(c1) :
  return  0.5+((np.sin(c1*(q**2)))**2-0.5)*(np.exp(-(c2*q)**gamma))
