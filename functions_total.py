import numpy as np 
import torch
from torch import tensor as t

y,x = np.indices((256,256))
q  = np.sqrt((x-127)**2+(y-127)**2)
q = q.astype(np.int) # ? 
qa = q.ravel()
nq = np.bincount(qa)
q = q.astype(np.float)

def azimuthalAverageFast(immarray) :
 """This function basically do the same as the other azimuthal average,
 but here the r matrix is passed as an argument, so that can be computed 
 only once"""
 tbin = np.bincount(qa,immarray.ravel())
 return tbin/nq

vectAv = np.vectorize(azimuthalAverageFast, signature = '(n,n)->(m)')

def q_filter(qro) : 
  return 1/(1 + (q/qro)**4)

def tf_filter(c1,c2,gamma) :
  return  0.5+((np.sin(c1*(q**2)))**2-0.5)*(np.exp(-(c2*q)**gamma))

def normalizeData(input_data,output_data,frange) :
  """I normalize input and output data based on the input data. The function takes 
  input and output data and returns them normalized. It also returns the normalization
  factor used for each data, useful to retrieve the correct scale in the end."""
  factors = t(np.zeros(len(input_data)),dtype = torch.float)
  factorso = t(np.zeros(len(input_data)),dtype = torch.float)
  inp = t(np.zeros((len(input_data),182)),dtype = torch.float)
  out = t(np.zeros((len(output_data),182)),dtype = torch.float)
  for i in range(len(input_data)) : 
    factors[i] = torch.max(input_data[i])-torch.min(input_data[i])
    factorso[i] = torch.max(input_data[i])-torch.min(input_data[i])
    mini = torch.min(input_data[i])
    mino = torch.min(output_data[i])
    inp[i] = (input_data[i] - mini)  / factors[i] * (frange[1] - frange[0]) + frange[0]
    out[i] = (output_data[i] -mino)/ factorso[i] * (frange[1] - frange[0]) + frange[0]

  return inp, out

def deNormalizeData(data,input_data,m,M) :
  for i in range(len(data)) : 
    maxi = torch.max(input_data[i])
    mini = torch.min(input_data[i])
    data[i] = ((data[i]-m)*(maxi-mini)/(M-m) +mini)

def weightedMSELoss(input_data,target,weights) :
    
    out = (target - input_data)**2
    out = out * weights.expand_as(out)
    loss = out.sum()
    return loss/len(target)

def MSELoss(input_data,target) :
    
    out = (target - input_data)**2
    loss = out.sum()
    return loss/len(out)

def percentMSELoss(input_data,target) :
    
    out = ((target - input_data)/(target+1e-5))**2
    loss = out.sum()
    return loss/out.flatten().size()[0]

def normalizeSingleData(input_data,output_data,frange) :
  """I normalize input and output data based on the input data. The function takes 
  input and output data and returns them normalized. It also returns the normalization
  factor used for each data, useful to retrieve the correct scale in the end."""
  factors = t(np.zeros(len(input_data)),dtype = torch.float)
  factorso = t(np.zeros(len(input_data)),dtype = torch.float)
  inp = t(np.zeros(len(input_data)),dtype = torch.float)
  out = t(np.zeros(len(output_data)),dtype = torch.float)
  factors = torch.max(input_data)-torch.min(input_data)
  factorso = torch.max(input_data)-torch.min(input_data)
  mini = torch.min(input_data)
  mino = torch.min(output_data)
  inp = (input_data - mini)  / factors * (frange[1] - frange[0]) + frange[0]
  out = (output_data -mino)/ factorso * (frange[1] - frange[0]) + frange[0]

  return inp, out


def deNormalizeSingleData(data,input_data,m,M) :
  maxi = torch.max(input_data)
  mini = torch.min(input_data)
  data = ((data-m)*(maxi-mini)/(M-m) +mini)
  return data
