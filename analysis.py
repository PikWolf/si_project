import torch
from torch import tensor as t
import numpy as np
import matplotlib.pyplot as plt
import functions_total as f
import model as md
import torch.nn as nn
import pandas as pd
from tqdm import tqdm 
from numpy.random import default_rng as rng
from numpy import fft

# instantiate basic variables

c1_min = float(input('Insert the minimum value for c1: '))
c1_max = float(input('Insert the maximum value for c1: '))
c2_min = float(input('Insert the minimum value for c2: '))
c2_max = float(input('Insert the maximum value for c2: '))
ga_min = float(input('Insert the minimum value for gamma: '))
ga_max = float(input('Insert the maximum value for gamma: '))

qro_min = float(input('Insert the minimum value for q_ro: '))
qro_max= float(input('Insert the maximum value for q_ro: '))

dnumber = 10

# the model 

model = md.model
model.load_state_dict(torch.load('model'))
model.eval()

# the accuracy

#accuracy = nn.MSELoss(reduction = 'mean') 
accuracy = f.weightedMSELoss
#weights = t(np.concatenate((np.ones(100),np.linspace(1,0,82))),dtype = torch.float)
#weights = t(np.linspace(1,0,182),dtype = torch.float)
#weights = t(np.ones(182),dtype = torch.float)
#weights = t(np.concatenate((np.ones(10), np.linspace(1,5,172)**(-4))))
weights = t(np.concatenate((np.ones(10), np.linspace(1,5,172)**(-4))))

### MEGA LOOP ###
# the dataframe
data = pd.DataFrame({'q_ro': np.zeros(int(dnumber**4*10)), 'c_1': np.zeros(int(dnumber**4*10)), 'c_2': np.zeros(int(dnumber**4*10)), 'gamma': np.zeros(int(dnumber**4*10)), 'error': np.zeros(int(dnumber**4*10))}) 

spectra = (np.zeros((dnumber**4*10,182)),np.zeros((dnumber**4*10,182)))

index = 0

for qr in tqdm(np.linspace(qro_min,qro_max,dnumber)) :
  for c1 in np.linspace(c1_min,c1_max,dnumber) :
    for c2 in np.linspace(c2_min,c2_max,dnumber) :
      for ga in np.linspace(ga_min,ga_max,dnumber) :
        for d in range(10) : 
          # create the data couple
          im = fft.fftshift(fft.fft2(rng().normal(0,1,(256,256))))
          target = (np.abs(im))**2 * f.q_filter(qr)
          input_ = target * f.tf_filter(c1,c2,ga)
          target_spectrum = t(f.azimuthalAverageFast(target),dtype = torch.float)
          input_spectrum = t(f.azimuthalAverageFast(input_),dtype = torch.float)
          spectra[0][index] = input_spectrum.detach().numpy()
          spectra[1][index] = target_spectrum.detach().numpy()
#          input_spectrum_n,target_spectrum_n = f.normalizeSingleData(input_spectrum,target_spectrum,(0.,1.))

          # compute the model prediction
          pred = model(input_spectrum)
          
          # denormalize
         # f.deNormalizeSingleData(pred,input_spectrum,0.,1.)

          # compute the error
          err = f.weightedMSELoss(pred,target_spectrum,weights)

          # fill the row in the dataframe
          data.iloc[index] = [qr,c1,c2,ga,err.detach().numpy()]
          
          # update the index
          index = index + 1

data.to_csv('data_analysis_final.csv')
np.savez('data_spectra_final.npz',spectra[0],spectra[1])
