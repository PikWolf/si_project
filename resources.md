## MIT Deep Learning book

[Link](http://www.deeplearningbook.org/)<br/>

## Introduction to ML and other resources

[From regression to NN](https://towardsdatascience.com/a-gentle-journey-from-linear-regression-to-neural-networks-68881590760e)<br/>

## Normalization

[Different types](https://mlexplained.com/2018/11/30/an-overview-of-normalization-methods-in-deep-learning/)<br/>
[on the same subject](http://mlexplained.com/2018/01/13/weight-normalization-and-layer-normalization-explained-normalization-in-deep-learning-part-2/)<br/>

## Intel mkl-dnn

[Link](https://github.com/intel/mkl-dnn)<br/>

## Weight initialization

[Pytorch examples](https://code-examples.net/en/q/2f24d50)<br/>

## Generic material

[37 common problems](https://blog.slavv.com/37-reasons-why-your-neural-network-is-not-working-4020854bd607)<br/>
[Importance of initializatin and momentum in deep learning](http://www.cs.toronto.edu/~hinton/absps/momentum.pdf)<br/>
[Regarding batch size](https://machinelearningmastery.com/gentle-introduction-mini-batch-gradient-descent-configure-batch-size/)<br/>

## Pytorch

[why it is cool](https://blog.paperspace.com/why-use-pytorch-deep-learning-framework/)<br/>
[Access weigths](https://stackoverflow.com/questions/44284506/pytorch-nn-sequential-access-weights-of-a-specific-module-in-nn-sequential)<br/>
[Custom Loss](https://stackoverflow.com/questions/44597523/custom-loss-function-in-pytorch)<br/>
[Tuning the fine structure of the net](https://stackoverflow.com/questions/59763208/is-there-a-way-to-remove-1-2-or-more-specific-neuron-connections-between-the-l)<br/>

## Activation functions

[Types](https://missinglink.ai/guides/neural-network-concepts/7-types-neural-network-activation-functions-right/)<br/>
[ReLU](https://machinelearningmastery.com/rectified-linear-activation-function-for-deep-learning-neural-networks/)<br/>

## Loss functions

[Cross entropy](https://rdipietro.github.io/friendly-intro-to-cross-entropy-loss/)<br/>

## Optimization algorithms

[Adam](https://medium.com/@nishantnikhil/adam-optimizer-notes-ddac4fd7218)<br/>
[Adam Article](https://arxiv.org/abs/1412.6980)<br/>

## Convolutional neural networks

[How they works](https://www.reddit.com/r/Python/comments/et970j/when_i_was_learning_machine_learning_for_the/)<br/>

## Avoid forgetting of the data

[stack overflow](https://stackoverflow.com/questions/33996467/how-to-prevent-nn-from-forgetting-old-data)<br/>
[importante articolo](https://www.pnas.org/content/pnas/114/13/3521.full.pdf)<br/>
