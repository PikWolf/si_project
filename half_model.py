import torch 
import torch.nn as nn
from torch.utils.data import Dataset as ds
from torch.nn import Sequential as sq


class GELU(nn.Module):
    def forward(self, input):
      return nn.functional.gelu(input)


# loss function
loss = torch.nn.MSELoss(reduction='mean')

model = sq(
          nn.Linear(91,3),
          nn.Linear(3,3),
          nn.Softplus(),
          nn.Linear(3,91),
	  )

# optimizer
#optimizer = torch.optim.SGD(model.parameters(),0.02,momentum = 0.83)
optimizer = torch.optim.Adam(model.parameters())
# dataset class

class dataset(ds) :
  def __init__(self,input_data,output_data) :
    self.targets = output_data
    self.inputs = input_data
    self.dict = {k  : input_data[k] for k in range(len(input_data))}
    self.dicto = {k  : output_data[k] for k in range(len(output_data))}

  def __getitem__(self,key) : 
    return self.dict[key], self.dicto[key]
  
  def __len__(self) :
    return len(self.targets)


