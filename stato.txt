Per ora ho:

- fatto una rete semplice che funziona molto bene se i dati sono banali (va beh, inutile)
- fatto una rete che è in grado di discriminare i segnali con diversa posizione del ginocchio. Per il resto
  l'ampiezza non è rilevante, nel senso che normalizzo i dati prima di processarli e li denormalizzo alla fine.
  Per farlo la rete è fatta di:
  - un layer lineare in input, che prende 182 valori in input e ha 5 valori di output (provato anche con 3, 10, etc: non cambia poi così tanto)
  - un layer hidden che prende 5 input e ha 5 output (stesso ragionamento) con funzione di attivazione (il layer successivo) softplus
  - Un layer lineare in output, che prende 5 input e ha 182 output. Questo per forza deve rimanere così, dato che comunque è un problema di
    regressione sostanzialmente.
  Succede che allenandolo con un dataset di 2000 esempi fatti da funzioni in cui i parametri della funzione di trasferimento E la posizione del ginocchio
  nel filtro q^-4 sono variabili entro certi range ragionevoli, la rete impara sempre la pendenza q^4, impara a discriminare la posizine del ginocchio, ma non 
  è molto precisa, nel senso che per i dati con ginocchio vicino a zero sbaglia di più.
  Inoltre, il grosso problema è la predizione per i valori ad alti q. Lì succedono i veri pasticci, perché si va su valori molto piccoli, attorno allo zero, e in scala 
  logaritmica si fanno casini (bastano piccoli valori negativi e ho una grande variabilità). Si vede ciò quando denormalizzo. Questo soprattutto per i dati con 
  ginocchio a q piccolo. 
  Per risolvere questo ho provato a dividere il problema usando due modelli: uno per metà range di q, uno per l'altra metà. Con scarsi risultati: succede la stessa cosa nel 
  primo range. 
  Che fare ora? provare a migliorare o no? La mia priorità ora è dare l'esame, pochi cazzi. Pooi si vedrà.


  In ogni caso Adam sembra superiore sempre

  allora: mettere un bias alla funzione di attivazione in effetti riesce a risolvere quel problema del rumore ad alti q. Però rovina la predeizione del ginocchio. mmh... come fare?

  Consiglio di Carpineti: prova a usare una sola funzione di trasferimento per capire se quello incide sulla perdita di precisione pre ginocchi bassi.

  IDEA: i parametri della funzione di trasferimento sono, per un dato setup, sempre gli stessi. E se, in un esperimento, si fa una misura senza gradiente, dove lo spettro
  è più piatto. Poi da quelle misure si cerca di capire i parametri e si usano per la fase succesiva. Grazie al cazzo, così il mio metodo perde di utilità.
  
  Nota: normalizzando tra -1 e 1 con sigmoid ho un risultato decente.

  Effettivamente con la stessa funzione di trasferimento va effettivamente meglio, cioè la rete predice bene fino allo stesso range per entrambe le posizioni dei ginocchi

  IDEE:
  - lavoro sui moduli e non sui moduli quadri, così ho meno problemi. Però la funzione di trasferimento?
  - normalizzo con una radice, in qualche modo
  - credo che il problema sia la funzione obiettivo: i valori piccoli hanno scarti piccoli che però in loglog sono enormi. Quindi per la rete sono irrilevanti e smette
    di imparare


 - sì, va un po' meglio così, però comunque non benissimo. 


dPlease, insert the path where you want to save the datasets: ati7
Insert the number of different values of c1 (i.e. z) for
the training dataset5
Insert the minimum value of c1: 0.1
Insert the maximum value of c1: 20
Insert the minimum value of c2: 0.01
Insert the maximum value of c2: 0.1
Insert the minimum value of gamma: 0.5
Insert the maximum value of gamma: 1.5
Now, regarding the target:
Insert the minimum value of qro: 2
Insert the minimum value of qro: 10
1 of 3...
2 of 3...
3 of 3...

con -1+1 e questo dataset non male,..con 5 neuroni nell'hidden layer

ora provo con 182 hidden layer
no, meglio con pochi.

con softplus: tra 0 e 1
mmh, ok, però non so..

ho provato a cambiare i pesi ma sembra che vada peggio

provo a tenere quesi pesi e a rimettere sigmoid, ma tra 0 e 1

ora ho messo dei pesi che variano durante l'apprendimento. In realtà, nonostante la loss function faccia cose pazze,
non sempra male. Però, non è un po' come far partire l'apprendimento da una posizine diversa?

ATTENZIONE: SEMBRA CHE CON PESI TUTTI 1 (CIOè NON PESI) SIA MEGLIO :')


riepilogo:
sto facendo un'analisi più accurata. Quel che vedo è che:
valutare, sul testset, l'errore in questo modo non ha molto senso, nel senso che inevitabilmente così vengono pesate di più
gli errori ad alti q_ro, perché, anche se in percentuale sono più bassi, in assoluto sono più grandi, perché i valori della
curva sono mediamente più grandi. Questo perché la rete si è allenata sulle curve normalizzate. Ma, in realtà, non è quello che
voglio. Possibili soluzioni:
- usare, in test, l'errore sulle immagini normalizzate, e non su quelle denormalizzate. Questo aiuta a valutare effettivamente
  le performance della rete. Tuttavia evidenzia che la rete non fa proprio quello che ci si aspetta.
- allenare la rete in modo che tenga conto della scala. Per esempio calcolando l'errore sugli spettri NON normalizzati. Tuttavia,
  attento, se la fai lavorare sugli spettri normalizzati, e poi le dici che sbaglia in base a quelli normalizzati, come fa
  a imparare?
- un'altra idea: usare come errore o valutazione di accuracy l'errore sul fit, dato che alla fine ci interessa solo la posizione del
  ginocchio e poco altro. mmh.. non so
- non normalizzo un cazzo, ma spampano la sigmoide e vediamo che succede

molto meglio così. Ora magari aggrega i valori di test con dei valori medi

mmh da qui vedo che non sembra esserci combinazione di parametri preferita. Cioè non vedo nessun trend. 

allora, come la mettiamo? 

inoltre sembra che i dati con errore minore, almeno calcolato in questo modo 

sono tornato a usare funzione costo pesata, ma pesata di più sulla prima parte. poi ho aumentato il numero
degli hidden di nuovo, a q0. Può essere che magari aumentando i lnumero di parametri allenabili, la rete approssimi
meglio, ma non è detto. Inoltre ora magari provo a usare minibatch più piccoli ancora, oppure, più grandi.

con batch più piccoli fa più cagare. vediamo con 512. sembra decente, ma ci mette molto di più, come era prevedibile.
ora provo a farlo andare a 1500 epoche. la vloss scende ancora..

si, con 2000 epoche va meglio, ma comunque mi butta giù dei dati maledettamente. IDEA: metto una relu, o softplus in uscita,
e vaffanculo.

il primo approccio fa schifo, vediamo se spampanando la softplus migliora. Comunque è risaputo che per problemi di regressione
in uscita bisognerebbe mettere unità lineari. Risposta: no.

ora provo a mettere softplus in ingresso, ma non vedo a cosa possa servire.

No, non migliora

ora provo ad aumentare ancora i batch e le epoche... vediamo. con 1024 batch size
ì, non male, con 2000, con 2500, ma continua a scendere... proviamo con 3000
ancora continua a scendere.
Sì, proviamo a 3500, e poi a 4000! facciamolo overfittare, questo disgraziato

Con 3500 continua a scendere. Però fa puttanate con i valori bassi... provo comunque ad andare a 4000, se va male torno tra
i 3000 e i 3500
per fortuna è andata bene. direi che continua a 4500. Scendere, scende ancora. Vediamo come va.
diminuisce, però fa un po' o solii casini. Io provo a aumentare il numero di hidden.
con 45 hidden (1 ogni 4 di input e output), dopo i 3000 comincia a overfittare, almeno guardando la vloss
overfitta ancora... e fa schifo. provo a fermarmi a 2500
sì ok, va un po' meglio ma ancora non come prima... torno a 32 hidden. vediamo
no.
provo 13:
con 4000 niente male devo dire


ATTENZIONE
c'è un problema. Essendo tornato alla situazione prenormalizzazione, rimane il problema che questo modello va bene solo 
per dati in cui si parte da rumore bianco con le stesse caratteristiche. cioè dati in cui lo spettro abbia la stessa ampiezza. O no?
magari, a esame dato, vedere se si riesce ad allenare anche su dati misti. Ma NON menzionare a Borghese, assolutamente. Se no mi dice
di provae.

comunque, per presentazione:
- intro
- descrizione del modello
- descrizione della procedura
- errore medio
- errore medio al variare dei parametri
- caso migliore, caso peggiore, un po' di casi
- 

