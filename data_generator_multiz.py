import numpy as np
from numpy import fft
import functions_multiz as f
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize  as nr

n_train = 1000
n_val = 500
n_test = 500
qro = 5
n_z = 10

print('1 of 3...')
# generating the ffts of random white noise images
trainset_output = fft.fftshift(fft.fft2(np.random.normal(0,1,(n_train,256,256))))
valset_output = fft.fftshift(fft.fft2(np.random.normal(0,1,(n_val,256,256))))
testset_output = fft.fftshift(fft.fft2(np.random.normal(0,1,(n_test,256,256))))
print('2 of 3...')
# filtering with q^-4 power law // question: should i filter on the 
# averaged data isntead of on the images? Don't think so
trainset_output = ((np.abs(trainset_output))**2 * f.q_filter(qro)).astype(np.float)
valset_output = ((np.abs(valset_output))**2 * f.q_filter(qro)).astype(np.float)
testset_output = ((np.abs(testset_output))**2 * f.q_filter(qro)).astype(np.float)

trainset_input = np.zeros((n_train,256,256))
valset_input = np.zeros((n_val,256,256))
testset_input = np.zeros((n_test,256,256))


N_z = int(n_train/n_z)
for s in range(n_z) :
  trainset_input[s*N_z:N_z+N_z*s] = trainset_output[s*N_z:N_z+N_z*s] * f.tf_filter(0.1+0.1*s)

N_z = int(n_val/n_z)
for s in range(n_z) :
  valset_input[s*N_z:N_z+N_z*s] = valset_output[s*N_z:N_z+N_z*s] * f.tf_filter(0.1+0.1*s)

N_z = int(n_train/n_z)
for s in range(n_z) :
  testset_input[s*N_z:N_z+N_z*s] = testset_output[s*N_z:N_z+N_z*s] * f.tf_filter(0.1+0.1*s)

print('3 of 3...')

# normalizing the data. This is important in order to not saturate 
# the sigmoids

#for i in range(len(trainset_output)) : 
#  trainset_output[i] = nr(trainset_output[i])
#  trainset_input[i] = nr(trainset_input[i])
#for i in range(len(valset_output)) : 
#  valset_output[i] = nr(valset_output[i])
#  valset_input[i] = nr(valset_input[i])
#for i in range(len(testset_output)) : 
#  testset_output[i] = nr(testset_output[i])
#  testset_input[i] = nr(testset_input[i])

np.savez('train.npz',f.vectAv(trainset_output),f.vectAv(trainset_input))
np.savez('val.npz',f.vectAv(valset_output),f.vectAv(valset_input))
np.savez('test.npz',f.vectAv(testset_output),f.vectAv(testset_input))

if __name__ == "__main__" : 
## debug 
  plt.loglog(f.azimuthalAverageFast(trainset_output[1,:,:]),'.-')
  plt.loglog(f.azimuthalAverageFast(trainset_input[1,:,:]),'.-')
  plt.show()
##
