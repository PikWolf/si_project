import model as md
import numpy as np 
import matplotlib.pyplot as plt
import functions_total as f 
from torch import tensor as t
import torch
from torch.utils.data import DataLoader as dl
from torch.utils.data import TensorDataset

traindata = np.load(input('Insert the path of the train data: '))
output_data = t(traindata['arr_0'],dtype = torch.float)
input_data = t(traindata['arr_1'],dtype = torch.float)
#input_data, output_data = f.normalizeData(input_data,output_data,(0.,1.))
traindataset = TensorDataset(input_data,output_data)
trainloader = dl(traindataset,batch_size = 1024 ,shuffle = True)

valdata = np.load(input('Insert the path of the val data: '))
output_data = t(valdata['arr_0'],dtype = torch.float)
input_data = t(valdata['arr_1'],dtype = torch.float)
#input_data, output_data= f.normalizeData(input_data,output_data,(0.,1.))
valdataset = TensorDataset(input_data,output_data)
valloader = dl(valdataset,batch_size = 1024)


epochs = int(input('Insert the epochs number: '))

loss = 0.0
losses = np.zeros(epochs)
vallosses = np.zeros(epochs)
#vloss = torch.nn.MSELoss(reduction='sum')
vloss = f.weightedMSELoss
#vloss = f.percentMSELoss
#weights = t(np.linspace(1,0.1,182)**4)
weights = t(np.concatenate((np.ones(10), np.linspace(1,5,172)**(-4))))

# training loop
for epoch in range(epochs) : 
  md.model.train()
  for i,batch in enumerate(trainloader,0) :
    x,targets = batch
    md.optimizer.zero_grad()
    
    # forward
    out = md.model(x)
   # loss = md.loss(out,targets)
    loss = f.weightedMSELoss(out,targets,weights)
   # loss = f.percentMSELoss(out,targets)
    if i % 1023 == 0 :
      print(loss)
    loss.backward()
    
    md.optimizer.step()
  losses[epoch] = loss

  md.model.eval()
  with torch.no_grad() :
    valid_loss = sum(vloss(md.model(x),y,weights) for x,y in valloader) 
    vallosses[epoch] = valid_loss
    if epoch % 10 == 0 :
      print(epoch,valid_loss/len(valloader))
#  if epoch < 150 and epoch > 50 :  
#    weights = t(np.logspace(1,10-0.1*(epoch-49),182))
#    print(torch.max(weights))

torch.save(md.model.state_dict(), 'model')
plt.plot(np.arange(epochs),losses,'-',label = 'train loss')
plt.plot(np.arange(epochs),vallosses,'-', label = 'validation loss')
plt.legend()
plt.show()
