import numpy as np
from numpy import fft
import functions_total as f
import matplotlib.pyplot as plt
from numpy.random import default_rng as rng

n_train = 5000
n_val = 2000
n_test = 2000

rg = rng

path = input('Please, insert the path where you want to save the datasets: ')

n_z = int(input('Insert the number of different values of c1 (i.e. z) for\nthe training dataset'))
z_min = float(input('Insert the minimum value of c1: '))
z_max = float(input('Insert the maximum value of c1: '))

c2_min = float(input('Insert the minimum value of c2: '))
c2_max = float(input('Insert the maximum value of c2: '))

gamma_min = float(input('Insert the minimum value of gamma: '))
gamma_max = float(input('Insert the maximum value of gamma: '))

print('Now, regarding the target:')
#mu_min = float(input('Insert the minimum value of mu: '))
#mu_max = float(input('Insert the minimum value of mu: '))

qro_min = float(input('Insert the minimum value of qro: '))
qro_max = float(input('Insert the minimum value of qro: '))

print('1 of 3...')
# generating the ffts of random white noise images
t_o = np.array([fft.fftshift(fft.fft2(rg().normal(0,1,(256,256)))) for a in range(n_train)])
v_o = np.array([fft.fftshift(fft.fft2(rg().normal(0,1,(256,256)))) for a in range(n_val)])
ts_o = np.array([fft.fftshift(fft.fft2(rg().normal(0,1,(256,256)))) for a in range(n_test)])
print('2 of 3...')
# filtering with q^-4 power law // question: should i filter on the 
# averaged data isntead of on the images? Don't think so
t_fil = np.array([f.q_filter(rg().uniform(qro_min,qro_max)) for g in range(n_train)])
v_fil = np.array([f.q_filter(rg().uniform(qro_min,qro_max)) for g in range(n_val)])
ts_fil = np.array([f.q_filter(rg().uniform(qro_min,qro_max)) for g in range(n_test)])

trainset_output = (np.abs(t_o))**2 * t_fil
valset_output = (np.abs(v_o))**2 * v_fil
testset_output = (np.abs(ts_o))**2 * ts_fil

del t_o,v_o,ts_o,t_fil,v_fil,ts_fil

trainset_input = np.zeros((n_train,256,256))
valset_input = np.zeros((n_val,256,256))
testset_input = np.zeros((n_test,256,256))


N_z = int(n_train/n_z)
z_step = (z_max - z_min)/n_z

for s in range(n_z) :
  trainset_input[s*N_z:N_z+N_z*s] = trainset_output[s*N_z:N_z+N_z*s] * f.tf_filter(z_min+z_step*s,rg().uniform(c2_min,c2_max),rg().uniform(gamma_min,gamma_max))

N_z = int(n_val/n_z)
for s in range(n_z) :
  valset_input[s*N_z:N_z+N_z*s] = valset_output[s*N_z:N_z+N_z*s] * f.tf_filter(z_min+z_step*s,rg().uniform(c2_min,c2_max),rg().uniform(gamma_min,gamma_max))

N_z = int(n_train/n_z)
for s in range(n_z) :
  testset_input[s*N_z:N_z+N_z*s] = testset_output[s*N_z:N_z+N_z*s] * f.tf_filter(z_min+z_step*s,rg().uniform(c2_min,c2_max),rg().uniform(gamma_min,gamma_max))

print('3 of 3...')

np.savez(path+'/train.npz',f.vectAv(trainset_output),f.vectAv(trainset_input))
np.savez(path+'/val.npz',f.vectAv(valset_output),f.vectAv(valset_input))
np.savez(path+'/test.npz',f.vectAv(testset_output),f.vectAv(testset_input))

if __name__ == "__main__" : 
## debug 
  plt.loglog(f.azimuthalAverageFast(trainset_output[1,:,:]),'.-')
  plt.loglog(f.azimuthalAverageFast(trainset_input[1,:,:]),'.-')
  plt.show()
##
