import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset as ds


m = nn.Sigmoid()
n = nn.Softplus()

class Net(nn.Module) :
  
  def __init__(self) : 
    super(Net, self).__init__()
    # input layer
    self.input = nn.Linear(182,13)
    # hidden layer
    self.hidden = nn.Linear(13,13)
    # output layer
    self.output = nn.Linear(13,182)
    
  def forward(self,x) :
    output1 = self.input(x)
#    print(torch.max(self.hidden(output1)))
    output2 = m(self.hidden(output1)/5e3)*5e3
    output = self.output(output2)
    return output

class dataset(ds) :
  def __init__(self,input_data,output_data) :
    self.targets = output_data
    self.inputs = input_data
    self.dict = {k  : input_data[k] for k in range(len(input_data))}
    self.dicto = {k  : output_data[k] for k in range(len(output_data))}

  def __getitem__(self,key) : 
    return self.dict[key], self.dicto[key]
  
  def __len__(self) :
    return len(self.targets)

model = Net()

#optimizer = torch.optim.SGD(model.parameters(),0.0182)
optimizer = torch.optim.Adam(model.parameters())
loss = torch.nn.MSELoss(reduction='sum')
